package com.khrystyna;

import com.khrystyna.model.Mobile;
import com.khrystyna.model.ReflectionService;
import com.khrystyna.model.User;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Application {
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        testReflectionService();
    }

    private static void testReflectionService() {
        User user = new User(23L, "svt", "alwaysgood32");
        Mobile mobile = new Mobile("+380637315152");

        ReflectionService service = new ReflectionService();

        service.printAnnotatedFields(user);
        service.getAllInfo(user);
        service.getAllInfo(mobile);
        service.invokeMethod(mobile, "sendSms", "sms-text",
                new String[]{"+380731515512", "other-one"});
    }
}
