package com.khrystyna.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ReflectionService {
    private static Logger logger = LogManager.getLogger(ReflectionService.class);

    public void printAnnotatedFields(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            CustomField annotation = field.getAnnotation(CustomField.class);
            if (!Objects.isNull(annotation)) {
                field.setAccessible(true);
                try {
                    Object value = field.get(object);
                    logger.trace("value = " + value);
                    logger.trace("annotation.columnName() = " + annotation.columnName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } finally {
                    field.setAccessible(false);
                }
            }
        }
    }

    public void invokeMethod(Object object, String methodName, Object... params) {
        for (Method method : object.getClass().getMethods()) {
            if (method.getName().equals(methodName)) {
                try {
                    if (method.isVarArgs()) {
                        List<Object> paramsList = new LinkedList<>(Arrays.asList(params));
                        List<Object> paramsToInvoke = new LinkedList<>();
                        for (Parameter parameter : method.getParameters()) {
                            if (parameter.isVarArgs()) {
                                Object first = paramsList.get(0);
                                if (first.getClass().isArray()) {
                                    paramsToInvoke.add(first);
                                    break;
                                }
                                Object[] byClass = createArrayByClass(first.getClass(), paramsList.size());
                                paramsList.toArray(byClass);
                                paramsToInvoke.add(byClass);
                                break;
                            } else {
                                Object removed = paramsList.remove(0);
                                paramsToInvoke.add(removed);
                            }
                        }
                        params = paramsToInvoke.toArray();
                    }
                    method.invoke(object, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private <T> T[] createArrayByClass(Class<T> clazz, int length) {
        @SuppressWarnings("unchecked")
        T[] arr = (T[]) Array.newInstance(clazz, length);
        return arr;
    }

    public void getAllInfo(Object object) {
        Class<?> aClass = object.getClass();
        logger.trace("Class name: " + aClass.getName());

        logger.trace("Constructors:");
        for (Constructor<?> constructor : aClass.getDeclaredConstructors()) {
            logger.trace(constructor.toString());
        }

        logger.trace("Fields:");
        for (Field field : aClass.getDeclaredFields()) {
            logger.trace(field.toString());
        }

        logger.trace("Methods:");
        for (Method method : aClass.getDeclaredMethods()) {
            logger.trace(method.toString());
        }
    }
}
