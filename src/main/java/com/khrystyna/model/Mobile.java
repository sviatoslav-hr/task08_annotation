package com.khrystyna.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Mobile {
    private static Logger logger = LogManager.getLogger(Mobile.class);
    private String number;

    private List<String> callHistory = new ArrayList<>();
    private List<SmsEntry> smsHistory = new ArrayList<>();

    public Mobile(String number) {
        if (validateNumber(number)) {
            this.number = number;
        } else {
            this.number = "+0000000000";
        }
    }

    public void call(String number) {
        if (validateNumber(number)) {
            logger.trace("Phoning to " + number + " from " + this.number);
            callHistory.add(number);
        } else {
            logger.trace("Incorrect number!");
        }
    }

    private boolean validateNumber(String number) {
        if (Objects.isNull(number) || number.length() < 10) {
            return false;
        } else return number.matches("\\+[0-9]+");
    }

    public void sendSms(String text, String... numbers) {
        for (String number : numbers) {
            if (validateNumber(number)) {
                logger.trace("Sms was send to " + number);
                smsHistory.add(new SmsEntry(number, text, LocalDateTime.now()));
            } else {
                logger.warn("Incorrect number: " + number);
            }
        }
    }

    class SmsEntry {
        private String phone;
        private String text;
        private LocalDateTime dateTime;

        private SmsEntry(String phone, String text, LocalDateTime dateTime) {
            this.phone = phone;
            this.text = text;
            this.dateTime = dateTime;
        }
    }
}


